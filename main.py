# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals
from google.cloud import texttospeech #< Google TTS Engine Library
from sumy.parsers.html import HtmlParser
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer as Summarizer # Uses SumyAPI for Lsa based summarization
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words
import random
import feedparser
import yaml
import time

# Handy function for loading values from a yaml document
def configLoad(config, value):
        rawYaml = open(config, 'r')
        return yaml.load(rawYaml)[value]



# Instantiates a client
client = texttospeech.TextToSpeechClient()

# Sumy settings
LANGUAGE = "english"
SENTENCES_COUNT = 5

# Settings for different quips to be said

# Openings are said before the title of the RSS feed (Example: "In news from TechCrunch")
openings=["In news from ", "Now, here's the latest news from "]
# Interjections are pushed inbetween articles from the same source
interjections=["In other news.", "Up Next",]
# Closers are spit out at the end of the podcast. 
closers=["That's all folks !","Thank you, and Good Bye!","And that's the news for today."]
# Script is created here as a blank string to be called later
script=""

# For loop to load the RSS feeds 
for source in configLoad('./config.yaml','sources'):
    # Adds a random opening to be said before the Feeds title
    script += str(random.choice(openings)) + str(source['title']) + '.' + "\n"
    # Gets 3 articles from the feed to summarize and read
    for url in feedparser.parse(source['url']).entries[:3]:
        #  Copypasted from sumy docmentation From what I know it does the actual summarization stuff using the API
        if __name__ == "__main__":
            parser = HtmlParser.from_url(url.link, Tokenizer(LANGUAGE))
            stemmer = Stemmer(LANGUAGE)
            summarizer = Summarizer(stemmer)
            summarizer.stop_words = get_stop_words(LANGUAGE)

            # for loop to add the articles to the script to be read
            for sentence in summarizer(parser.document, SENTENCES_COUNT):
                script += str(sentence)
        # Adds a random interjection to be read at the end of the podcast. 
        script += "\n" + str(random.choice(interjections)) + "\n"
# Adds a random signoff from the array defined above
script += str(random.choice(closers))

# Google speech API Stuff Below...

chunkedscript=[script[i:i+4999] for i in range(0, len(script), 4999)]

for chunk in chunkedscript:
    print(chunk)
    # Set the text input to be synthesized
    synthesis_input = texttospeech.types.SynthesisInput(text=str(chunk))

    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.types.VoiceSelectionParams(
        language_code='en-US',
        ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)

    # Select the type of audio file you want returned
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)

    # Perform the text-to-speech request on the text input with the selected
    #  voice parameters and audio file type
    response = client.synthesize_speech(synthesis_input, voice, audio_config)

    # The response's audio_content is binary.
    with open('output.mp3', 'ab') as out:
        # Write the response to the output file.
        out.write(response.audio_content)
        out.close() 
        time.sleep(30)
print('Audio content written to file "output.mp3"')
