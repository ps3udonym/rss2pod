# RSS2POD

## How to use this

1. Download the python requirements 
	``` pip3 install -r requirements.txt ```
2. From here you should be able to run the script
3. (OPTIONAL) For converting to an MP3 using local TTS You will need the lame encoder

### To Use Espeak/Festival
	``` EXAMPLE: python3 main.py | espeak -ven-us+m2 --stdout >> pod.wav && lame pod.wav pod.mp3```
### To Use Google's TTS API
1. remove lines 60 and 63 from main.py with their respective comments
2. create a service account
3. enable the google TTS API
4. get the json key for the service account
5. place that key in any user accessible directory
6. export GOOGLE_APPLICATION_CREDENTIALS to the path of your json file

	```export GOOGLE_APPLICATION_CREDENTIALS="/direct/path/to/dir/[FILE_NAME].json"``` 

7. Run the script
8. play output.mp3

### Hopes and Dreams

I would like to do the following with this when I have time

* Put this process in gitlab CI and autopublish new episodes daily
* Use Gitlab Pages to publish shownotes and run the RSS feed for published podcasts
* Find a reliable way to store old "Episodes" (Something like an S3 Bucket)
* Build out multiple pipelines for different topics (Security, Devops, Gaming, Etc...)
